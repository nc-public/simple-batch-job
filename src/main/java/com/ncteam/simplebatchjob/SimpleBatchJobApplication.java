package com.ncteam.simplebatchjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleBatchJobApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleBatchJobApplication.class, args);
	}

}

package com.ncteam.simplebatchjob.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ncteam.simplebatchjob.entity.UserBean;

public interface UserRepository extends JpaRepository<UserBean, Integer>{

}

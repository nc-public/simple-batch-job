package com.ncteam.simplebatchjob.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import com.ncteam.simplebatchjob.entity.UserBean;
import com.ncteam.simplebatchjob.repo.UserRepository;

@Configuration
@EnableBatchProcessing
public class BatchConfig {
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private UserRepository userRepo;
	
	@Bean
	public FlatFileItemReader<UserBean> userReader(){
		FlatFileItemReader<UserBean> itemReader = new FlatFileItemReader<>();
		itemReader.setName("csv-reader");
		itemReader.setResource(new FileSystemResource("src/main/resources/customer-data.csv"));
		itemReader.setLinesToSkip(1);
		itemReader.setLineMapper(lineMapper());
		return itemReader;
	}

	private LineMapper<UserBean> lineMapper() {
		DefaultLineMapper<UserBean> lineMapper = new DefaultLineMapper<>();
		
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setDelimiter(",");
		lineTokenizer.setNames("cutomerID","first_name",	"last_name","company_name",	"address",	"city",	"county",	"state",	"zip",	"phone1",	"phone2",	"email",	"web");
		lineTokenizer.setStrict(false);
		
		BeanWrapperFieldSetMapper<UserBean> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(UserBean.class);
		
		lineMapper.setFieldSetMapper(fieldSetMapper);
		lineMapper.setLineTokenizer(lineTokenizer);
		return lineMapper;
	}
	
	@Bean
	public CustomUserProcesser userProcessor() {
		return new CustomUserProcesser();
	}
	@Bean
	public RepositoryItemWriter<UserBean> userWriter(){
		RepositoryItemWriter<UserBean> itemWriter = new RepositoryItemWriter<>();
		itemWriter.setRepository(userRepo);
		return itemWriter;
	}
	@Bean
	public Step step1() {
		return stepBuilderFactory.get("csv-step").<UserBean, UserBean>chunk(10).reader(userReader())
				.processor(userProcessor()).writer(userWriter()).build();
				
	}
	@Bean
	public Job job() {
		return jobBuilderFactory.get("csv-job").flow(step1()).end().build();
	}
}
